﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="23008000">
	<Property Name="CCSymbols" Type="Str"></Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Property Name="varPersistentID:{1B81401E-A519-479E-9BD4-F0B537E56F56}" Type="Ref">/My Computer/Variables_Library.lvlib/Variable1</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="SubVIs" Type="Folder">
			<Item Name="AddNewConnection.vi" Type="VI" URL="../SubVIs/AddNewConnection.vi"/>
			<Item Name="RandomString.vi" Type="VI" URL="../SubVIs/RandomString.vi"/>
			<Item Name="Launch_TCP_Instance_server.vi" Type="VI" URL="../SubVIs/Launch_TCP_Instance_server.vi"/>
			<Item Name="Update_TCP_Instance_Status.vi" Type="VI" URL="../SubVIs/Update_TCP_Instance_Status.vi"/>
			<Item Name="TCP_Instance_Status_2_String.vi" Type="VI" URL="../SubVIs/TCP_Instance_Status_2_String.vi"/>
			<Item Name="List Connections.vi" Type="VI" URL="../SubVIs/List Connections.vi"/>
			<Item Name="Close Stopped Connections.vi" Type="VI" URL="../SubVIs/Close Stopped Connections.vi"/>
			<Item Name="Remove Stopped Connections from List.vi" Type="VI" URL="../SubVIs/Remove Stopped Connections from List.vi"/>
		</Item>
		<Item Name="typedefs" Type="Folder">
			<Item Name="APP_Control_msg.ctl" Type="VI" URL="../typedefs/APP_Control_msg.ctl"/>
			<Item Name="APP_Control_msg_type.ctl" Type="VI" URL="../typedefs/APP_Control_msg_type.ctl"/>
			<Item Name="Connection.ctl" Type="VI" URL="../typedefs/Connection.ctl"/>
			<Item Name="Data_Processing_state.ctl" Type="VI" URL="../typedefs/Data_Processing_state.ctl"/>
			<Item Name="Data_Processing_state_type.ctl" Type="VI" URL="../typedefs/Data_Processing_state_type.ctl"/>
			<Item Name="Data_request.ctl" Type="VI" URL="../typedefs/Data_request.ctl"/>
			<Item Name="Data_request_type.ctl" Type="VI" URL="../typedefs/Data_request_type.ctl"/>
			<Item Name="HMI_Controls_refs.ctl" Type="VI" URL="../typedefs/HMI_Controls_refs.ctl"/>
			<Item Name="main_HMI_state.ctl" Type="VI" URL="../typedefs/main_HMI_state.ctl"/>
			<Item Name="main_HMI_state_type.ctl" Type="VI" URL="../typedefs/main_HMI_state_type.ctl"/>
			<Item Name="NewConnection.ctl" Type="VI" URL="../typedefs/NewConnection.ctl"/>
			<Item Name="TCP_Instance_Server_state.ctl" Type="VI" URL="../typedefs/TCP_Instance_Server_state.ctl"/>
			<Item Name="TCP_Instance_Server_state_type.ctl" Type="VI" URL="../typedefs/TCP_Instance_Server_state_type.ctl"/>
			<Item Name="TCP_Master_Server(listener)_state.ctl" Type="VI" URL="../typedefs/TCP_Master_Server(listener)_state.ctl"/>
			<Item Name="TCP_Master_Server(listener)_state_type.ctl" Type="VI" URL="../typedefs/TCP_Master_Server(listener)_state_type.ctl"/>
			<Item Name="TCP_Master_Server_state.ctl" Type="VI" URL="../typedefs/TCP_Master_Server_state.ctl"/>
			<Item Name="TCP_Master_Server_state_type.ctl" Type="VI" URL="../typedefs/TCP_Master_Server_state_type.ctl"/>
			<Item Name="TCP_Instance_Server_status_type.ctl" Type="VI" URL="../typedefs/TCP_Instance_Server_status_type.ctl"/>
			<Item Name="TCP_Instance_Server_status.ctl" Type="VI" URL="../typedefs/TCP_Instance_Server_status.ctl"/>
		</Item>
		<Item Name="TCP" Type="Folder">
			<Item Name="TCP_DataRequest.ctl" Type="VI" URL="../TCP/TCP_DataRequest.ctl"/>
			<Item Name="Read TCP Request.vi" Type="VI" URL="../TCP/Read TCP Request.vi"/>
			<Item Name="Data to TCP.ctl" Type="VI" URL="../TCP/Data to TCP.ctl"/>
			<Item Name="Send TCP Data.vi" Type="VI" URL="../TCP/Send TCP Data.vi"/>
			<Item Name="Send TCP Request.vi" Type="VI" URL="../TCP/Send TCP Request.vi"/>
			<Item Name="Read TCP Data.vi" Type="VI" URL="../TCP/Read TCP Data.vi"/>
		</Item>
		<Item Name="_main_HMI_LOOP.vi" Type="VI" URL="../_main_HMI_LOOP.vi"/>
		<Item Name="DataProcessing_LOOP.vi" Type="VI" URL="../DataProcessing_LOOP.vi"/>
		<Item Name="TCP_Master_Server_LOOP.vi" Type="VI" URL="../TCP_Master_Server_LOOP.vi"/>
		<Item Name="TCP_Instance_Server_LOOP.vi" Type="VI" URL="../TCP_Instance_Server_LOOP.vi"/>
		<Item Name="template_LOOP.vi" Type="VI" URL="../template_LOOP.vi"/>
		<Item Name="Simple_Data_Client.vi" Type="VI" URL="../Simple_Data_Client.vi"/>
		<Item Name="Launch Multiple TCP Clients.vi" Type="VI" URL="../Launch Multiple TCP Clients.vi"/>
		<Item Name="Shared_Variables.vi" Type="VI" URL="../Shared_Variables.vi"/>
		<Item Name="Variables_Library.lvlib" Type="Library" URL="../Variables_Library.lvlib"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="NI_MABase.lvlib" Type="Library" URL="/&lt;vilib&gt;/measure/NI_MABase.lvlib"/>
				<Item Name="TCP Listen.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Internecine Avoider.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/Internecine Avoider.vi"/>
				<Item Name="TCP Listen List Operations.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen List Operations.ctl"/>
				<Item Name="TCP Listen Internal List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen Internal List.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
			</Item>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
		</Item>
		<Item Name="Build Specifications" Type="Build"/>
	</Item>
</Project>
